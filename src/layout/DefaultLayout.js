import React, { Suspense } from 'react'
import { AppHeader } from '../components/index'
import Home from '../page/home'
import { CContainer, CSpinner } from '@coreui/react'
const DefaultLayout = () => {
  return (
    <div>
      <div className="wrapper d-flex flex-column min-vh-100">
        <AppHeader />
        <div className="body flex-grow-1">
          <CContainer className="px-4" lg>
            <Suspense fallback={<CSpinner color="primary" />}>
              <Home />
            </Suspense>
          </CContainer>
        </div>
      </div>
    </div>
  )
}

export default DefaultLayout
