/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react'
import { CCardBody, CFormSelect } from '@coreui/react'

// eslint-disable-next-line react/prop-types
const MonthPicker = ({ onChange, resetKey }) => {

  const months = [
    { value: '', label: 'Seleccione un Mes ...' },
    { value: '01', label: 'Enero' },
    { value: '02', label: 'Febrero' },
    { value: '03', label: 'Marzo' },
    { value: '04', label: 'Abril' },
    { value: '05', label: 'Mayo' },
    { value: '06', label: 'Junio' },
    { value: '07', label: 'Julio' },
    { value: '08', label: 'Agosto' },
    { value: '09', label: 'Septiembre' },
    { value: '10', label: 'Octubre' },
    { value: '11', label: 'Noviembre' },
    { value: '12', label: 'Diciembre' },
  ]

  const [selectedMonth, setSelectedMonth] = useState('')
  const [availableMonths, setAvailableMonths] = useState([])

  useEffect(() => {
    const currentDate = new Date()
    const currentMonth = currentDate.getMonth()
    const availableMonths = []

    for (let i = currentMonth; i >= 0; i--) {
      const monthValue = (i + 1).toString().padStart(2, '0')
      availableMonths.push(monthValue)
    }

    const currentMonthValue = (currentMonth + 1).toString().padStart(2, '0')
    availableMonths.push(currentMonthValue)
    setAvailableMonths(availableMonths)
  }, [])

  useEffect(() => {
    setSelectedMonth('')
  },[resetKey])

  const handleMonthChange = (event) => {
    setSelectedMonth(event.target.value)
    onChange(event.target.value)
  }

  return (
    <CCardBody style={{margin:"10px 0"}}>
      <CFormSelect aria-label="Select a month" value={selectedMonth} onChange={handleMonthChange}>
        {months.map((month) => (
          <option
            key={month.value}
            value={month.value}
            disabled={!availableMonths.includes(month.value)}
          >
            {month.label}
          </option>
        ))}
      </CFormSelect>
    </CCardBody>
  )
}

export default MonthPicker
