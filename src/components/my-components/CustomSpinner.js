/* eslint-disable prettier/prettier */
import React from 'react';
import '../my-styles/styleSpinner.css';

const CustomSpinner = () => {
  return (
    <div className="loading">Loading&#8230;</div>
  );
};

export default CustomSpinner;
