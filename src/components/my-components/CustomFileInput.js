/* eslint-disable prettier/prettier */
import React from 'react'
import PropTypes from 'prop-types'
import { CButton } from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilArrowThickFromBottom } from '@coreui/icons'
import '../my-styles/styleFileImput.css'

class CustomFileInput extends React.Component {
  static propTypes = {
    onFileSelected: PropTypes.func.isRequired,
    label: PropTypes.string,
    multiple: PropTypes.bool,
    disabled: PropTypes.bool,
  }

  handleFileInputChange = (event) => {
    const file = event.target.files[0]
    if (file) {
      this.props.onFileSelected(file)
      this.setState({ fileName: file.name })
    }
  }

  handleFileInputClick = () => {
    this.fileInput.click()
  }

  render() {
    const { label, multiple, disabled} = this.props

    return (
      <div className="content">
        <input
          className="input-file"
          id="my-file"
          type="file"
          onChange={this.handleFileInputChange}
          ref={(input) => {
            this.fileInput = input
          }}
          hidden
          multiple={multiple}
          accept=".xlsx, .xls"
          // disabled={disabled}
        />
        <CButton
          className="input-file-trigger"
          onClick={this.handleFileInputClick}
          tabIndex="0"
          size="lg"
          disabled={disabled}
        >
          {label} <CIcon icon={cilArrowThickFromBottom} title="Download file" />
        </CButton>

      </div>
    )
  }
}

export default CustomFileInput
