import PropTypes from 'prop-types'
import React, { useState, useEffect } from 'react'
import {
  CButton,
  CCallout,
  CCol,
  CForm,
  CFormInput,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CNav,
  CNavItem,
  CNavLink,
  CRow,
  CTabContent,
  CTabPane,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
  CToaster,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilArrowThickToBottom, cilClipboard } from '@coreui/icons'
import moment from 'moment/moment'
import CustomButton from './CustomButtom'
import { ProjectConst } from 'src/config'
import CustomToastComponent from './CustomToastComponent'

const CustomTabsVaccine = (props) => {
  const { tabContentClassName, data } = props

  const [toastVisible, setToastVisible] = useState(false)
  const [toastMessage, setToastMessage] = useState('')
  const [toastColor, setToastColor] = useState('')

  const [activeIndex, setActiveIndex] = useState(0)
  const [nameVaccine, setNameVaccine] = useState(data.length > 0 ? data[0][0].name : '')

  const [dataVaccines, setDataVaccines] = useState(data)

  const handleNavItemClick = (index, name) => {
    setActiveIndex(index)
    setNameVaccine(name)
  }

  const handleButtonClickDownload = () => {
    if (nameVaccine) {
      downloadExcel(nameVaccine)
    }
  }

  const downloadExcel = async (name) => {
    try {
      const formData = new FormData()
      formData.append('name', name)

      const respuesta = await fetch(`${ProjectConst.projectsUrl}/vaccines/excel`, {
        method: 'POST',
        body: formData,
      })
      if (respuesta.ok) {
        const base64Data = await respuesta.text()
        const linkSource = `data:application/vnd.ms-excel;base64,${base64Data}`
        const downloadLink = document.createElement('a')
        const fileName = `${nameVaccine}.xlsx`
        downloadLink.href = linkSource
        downloadLink.download = fileName
        downloadLink.click()
        setToastVisible(true)
        setToastMessage('Archivo Descargado')
        setToastColor('success')
      } else {
        console.log('La respuesta no fue exitosa:', respuesta.status)
      }
    } catch (e) {
      console.log('algo salio mal', e)
    }
  }

  const HandleSaveVaccine = () => {
    if (selectedItemUp) {
      saveOneDataVaccine(selectedItemUp)
    }
  }

  const saveOneDataVaccine = async (item) => {
    try {
      const formData = new FormData()
      formData.append('id', item.id)
      formData.append('name', item.name)
      formData.append('vaccineDate', item.vaccineDate)
      formData.append('previousBalance', item.previousBalance)
      formData.append('allIncome', item.allIncome ? item.allIncome : 0)
      formData.append('fa', item.fa)
      formData.append('fr', item.fr)
      formData.append('fm', item.fm)
      formData.append('fe', item.fe)
      formData.append('ff', item.ff)

      const response = await fetch(`${ProjectConst.projectsUrl}/vaccines/${item.id}`, {
        method: 'PUT',
        body: formData,
      })
      if (response.ok) {
        const data = await response.json()
        setDataVaccines(data)
        setToastVisible(true)
        setModalVisible(false)
        setSelectedItemUp(null)
        setToastMessage('Datos almacenados correctamente')
        setToastColor('success')
      } else {
        console.log('La respuesta no fue exitosa:', response.status)
      }
    } catch (error) {
      console.log('Algo salió mal:', error)
    }
  }

  const [modalVisible, setModalVisible] = useState(false)
  const [selectedItemUp, setSelectedItemUp] = useState(null)

  const toggleModal = (item) => {
    setModalVisible(!modalVisible)
    setSelectedItemUp({
      id: item.id,
      name: item.name,
      vaccineDate: item.vaccineDate,
      previousBalance: item.previousBalance,
      allIncome: 0,
      fa: 0,
      fr: 0,
      fm: 0,
      fe: 0,
      ff: 0,
    })
  }

  const closeModal = () => {
    setModalVisible(false)
    setSelectedItemUp(null)
  }

  const handleCloseToast = () => {
    setToastVisible(false)
  }

  useEffect(() => {
    setDataVaccines(data)
    setNameVaccine(data.length > 0 ? data[0][0].name : '')
  }, [data])

  let totalIncome = 0
  let totalExpences = 0
  let totalPO = 0
  let totalPNO = 0
  let totalAllAmountLost = 0

  dataVaccines[activeIndex]?.forEach((item) => {
    totalIncome += item.allIncome
    totalExpences += item.expenses
    totalPO += item.po
    totalPNO += item.pno
    totalAllAmountLost += item.allAmountLost
  })

  return (
    <div>
      {toastVisible && (
        <CToaster placement="top-end">
          <CustomToastComponent
            message={toastMessage}
            color={toastColor}
            visible={toastVisible}
            onClose={handleCloseToast}
          />
        </CToaster>
      )}
      <CNav variant="underline-border">
        {Array.isArray(dataVaccines) ? (
          dataVaccines.map((item, index) => (
            <CNavItem key={index}>
              <CNavLink
                href="#"
                active={index === activeIndex}
                onClick={() => handleNavItemClick(index, item[0].name)}
              >
                <CIcon icon={cilClipboard} className="me-2" />
                {item[0].name}
              </CNavLink>
            </CNavItem>
          ))
        ) : (
          <p></p>
        )}
      </CNav>
      {data.length > 0 ? (
        <CTabContent className={`rounded-bottom ${tabContentClassName ? tabContentClassName : ''}`}>
          <CTabPane className="p-3 preview" visible>
            <CRow className="g-3">
              <CCol xs>
                <p className="text-body-secondary small">Descargar el Excel de cada Vacuna:</p>
              </CCol>
              <CCol xs className="d-flex justify-content-end">
                <CustomButton name="Excel" onClick={handleButtonClickDownload}>
                  <CIcon icon={cilArrowThickToBottom} title="donwload" />
                </CustomButton>
              </CCol>
            </CRow>

            <CTable striped>
              <CTableHead className="!table-dark">
                <CTableRow>
                  <CTableHeaderCell scope="col">#</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Fecha</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Saldo Anterior</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Total Ingresos</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Cantidad Disponible</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Egresos</CTableHeaderCell>
                  <CTableHeaderCell scope="col">PO</CTableHeaderCell>
                  <CTableHeaderCell scope="col">PNO</CTableHeaderCell>
                  <CTableHeaderCell scope="col">FA</CTableHeaderCell>
                  <CTableHeaderCell scope="col">FR</CTableHeaderCell>
                  <CTableHeaderCell scope="col">FM</CTableHeaderCell>
                  <CTableHeaderCell scope="col">FE</CTableHeaderCell>
                  <CTableHeaderCell scope="col">FF</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Total Cantidad Perdida</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Saldo</CTableHeaderCell>
                </CTableRow>
              </CTableHead>
              <CTableBody>
                {Array.isArray(dataVaccines) ? (
                  dataVaccines[activeIndex]?.map((item, index) => (
                    <CTableRow key={index} onDoubleClick={() => toggleModal(item)}>
                      <CTableHeaderCell scope="row">{index + 1}</CTableHeaderCell>
                      <CTableDataCell>
                        {moment(item.vaccineDate).format('DD/MM/yyyy')}
                      </CTableDataCell>
                      <CTableDataCell className="text-center">
                        {item.previousBalance}
                      </CTableDataCell>
                      <CTableDataCell className="text-center">{item.allIncome}</CTableDataCell>
                      <CTableDataCell className="text-center">
                        {item.quantityAvailable}
                      </CTableDataCell>
                      <CTableDataCell className="text-center">{item.expenses}</CTableDataCell>
                      <CTableDataCell>{item.po}</CTableDataCell>
                      <CTableDataCell>{item.pno}</CTableDataCell>
                      <CTableDataCell>{item.fa}</CTableDataCell>
                      <CTableDataCell>{item.fr}</CTableDataCell>
                      <CTableDataCell>{item.fm}</CTableDataCell>
                      <CTableDataCell>{item.fe}</CTableDataCell>
                      <CTableDataCell>{item.ff}</CTableDataCell>
                      <CTableDataCell className="text-center">{item.allAmountLost}</CTableDataCell>
                      <CTableDataCell scope="row">{item.balance}</CTableDataCell>
                    </CTableRow>
                  ))
                ) : (
                  <p>NO HAY DATOS</p>
                )}
                <CTableRow>
                  <CTableDataCell className="text-center" colSpan={3} scope="row">
                    TOTAL MES
                  </CTableDataCell>
                  <CTableDataCell className="text-center" scope="row">
                    {totalIncome}
                  </CTableDataCell>
                  <CTableDataCell className="text-center"></CTableDataCell>
                  <CTableDataCell className="text-center" scope="row">
                    {totalExpences}
                  </CTableDataCell>
                  <CTableDataCell scope="row">{totalPO}</CTableDataCell>
                  <CTableDataCell scope="row">{totalPNO}</CTableDataCell>
                  <CTableDataCell className="text-center"></CTableDataCell>
                  <CTableDataCell className="text-center"></CTableDataCell>
                  <CTableDataCell className="text-center"></CTableDataCell>
                  <CTableDataCell className="text-center"></CTableDataCell>
                  <CTableDataCell className="text-center"></CTableDataCell>
                  <CTableDataCell className="text-center" scope="row">
                    {totalAllAmountLost}
                  </CTableDataCell>
                  <CTableDataCell scope="row"></CTableDataCell>
                </CTableRow>
              </CTableBody>
            </CTable>
          </CTabPane>
        </CTabContent>
      ) : (
        <CCallout color="warning" style={{ textAlign: 'center' }}>
          No Hay Datos
        </CCallout>
      )}

      <CModal
        backdrop="static"
        visible={modalVisible}
        onClose={closeModal}
        alignment="center"
        size="lg"
      >
        <CModalHeader>
          <CModalTitle style={{ sixe: '10px' }}>
            Datos de la Vacuna {selectedItemUp?.name}{' '}
            {moment(selectedItemUp?.vaccineDate).format('DD MMMM yyyy')}
          </CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CForm>
            <CRow className="mb-3">
              {moment(selectedItemUp?.vaccineDate).format('DD') === '01' ? (
                <CCol xs={4}>
                  <CFormInput
                    type="number"
                    id={'previousBalance'}
                    label="Saldo Anterior"
                    value={selectedItemUp?.previousBalance ?? 0}
                    onChange={(e) =>
                      setSelectedItemUp({ ...selectedItemUp, previousBalance: e.target.value })
                    }
                  />
                </CCol>
              ) : null}

              <CCol xs={4}>
                <CFormInput
                  type="number"
                  id={'allIncome'}
                  label="Ingresos"
                  value={selectedItemUp?.allIncome ?? 0}
                  onChange={(e) =>
                    setSelectedItemUp({ ...selectedItemUp, allIncome: e.target.value })
                  }
                />
              </CCol>
              <CCol xs={4}>
                <CFormInput
                  type="number"
                  id={'fa'}
                  label="F.A."
                  value={selectedItemUp?.fa ?? 0}
                  onChange={(e) => setSelectedItemUp({ ...selectedItemUp, fa: e.target.value })}
                />
              </CCol>
              <CCol xs={4}>
                <CFormInput
                  type="number"
                  id={'fr'}
                  label="F.R."
                  value={selectedItemUp?.fr ?? 0}
                  onChange={(e) => setSelectedItemUp({ ...selectedItemUp, fr: e.target.value })}
                />
              </CCol>
              <CCol xs={4}>
                <CFormInput
                  type="number"
                  id={'fm'}
                  label="F.M."
                  value={selectedItemUp?.fm ?? 0}
                  onChange={(e) => setSelectedItemUp({ ...selectedItemUp, fm: e.target.value })}
                />
              </CCol>
              <CCol xs={4}>
                <CFormInput
                  type="number"
                  id={'fe'}
                  label="F.E."
                  value={selectedItemUp?.fe ?? 0}
                  onChange={(e) => setSelectedItemUp({ selectedItemUp, fe: e.target.value })}
                />
              </CCol>
              <CCol xs={4}>
                <CFormInput
                  type="number"
                  id={'ff'}
                  label="F.F."
                  value={selectedItemUp?.ff ?? 0}
                  onChange={(e) => setSelectedItemUp({ selectedItemUp, ff: e.target.value })}
                />
              </CCol>
            </CRow>
          </CForm>
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={closeModal}>
            Cerrar
          </CButton>
          <CButton color="primary" onClick={HandleSaveVaccine}>
            Guardar Cambios
          </CButton>
        </CModalFooter>
      </CModal>
    </div>
  )
}

CustomTabsVaccine.propTypes = {
  tabContentClassName: PropTypes.string,
  data: PropTypes.array,
}

export default React.memo(CustomTabsVaccine)
