/* eslint-disable prettier/prettier */
import React, { useEffect, useState } from 'react'
import { CToast, CToastBody, CToastClose} from '@coreui/react'

// eslint-disable-next-line react/prop-types
const CustomToastComponent = ({ message, color, onClose }) => {
    const [visible, setVisible] = useState(true);

    useEffect(() => {
      const timer = setTimeout(() => {
        setVisible(false);
         onClose(); 
      }, 3000);
  
      return () => clearTimeout(timer); 
    }, [onClose]);
  
    return (
      <CToast
        autohide={false}
        visible={visible}
        color={color}
        className="text-white align-items-center"
      >
        <div className="d-flex">
          <CToastBody>{message}</CToastBody>
          <CToastClose className="me-2 m-auto" white onClick={onClose}/>
        </div>
      </CToast>
    )
}

export default CustomToastComponent
