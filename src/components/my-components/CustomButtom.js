/* eslint-disable prettier/prettier */
import React from 'react';
import PropTypes from 'prop-types';
import { CButton } from '@coreui/react';


const CustomButton = ({ name, children, disabled,onClick, }) => {
  return (
    <div className="input-file-container">
      <CButton
        color="success"
        className="input-file-trigger"
        tabIndex="0"
        size="lg"
        disabled={disabled}
        onClick={onClick}
      >
        {name} {children}
      </CButton>
    </div>
  );
};

CustomButton.propTypes = {
  name: PropTypes.string.isRequired,
  children: PropTypes.node,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
};

export default CustomButton;
