/* eslint-disable prettier/prettier */
import React, { useState } from 'react'
import {
  CCallout,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CToaster,
} from '@coreui/react'
import CustomFileInput from 'src/components/my-components/CustomFileInput'
import CIcon from '@coreui/icons-react'
import { cilSearch } from '@coreui/icons'
import MonthPicker from 'src/components/my-components/MonthPicker'
import CustomButton from 'src/components/my-components/CustomButtom'
import { ProjectConst } from '../config'
import moment from 'moment/moment'
import 'moment/locale/es'
import CustomTabsVaccine from 'src/components/my-components/CustomTabsVaccine'
import CustomToastComponent from 'src/components/my-components/CustomToastComponent'
import Spinner from '../components/my-components/CustomSpinner'

const Tables = () => {
  const [toastVisible, setToastVisible] = useState(false)
  const [toastMessage, setToastMessage] = useState('')
  const [toastColor, setToastColor] = useState('')

  const [selectedMonth, setSelectedMonth] = useState('')
  const [showMonth, setShowMonth] = useState('')

  const [listMonthVaccines, setListMonthVaccines] = useState([])

  const [selectedMonthUp, setSelectedMonthUp] = useState('')

  const [loading, setLoading] = useState(false)

  const [resetKey, setResetKey] = useState(false)
  const [resetKeyMonth, setResetKeyMonth] = useState(false)


  const handleMonthChangeUp = (newMonth) => {
    setSelectedMonthUp(newMonth)
  }

  const handleMonthChange = (newMonth) => {
    setSelectedMonth(newMonth)
    const monthMoment = moment(newMonth, 'MM')
    if (monthMoment.isValid()) {
      setShowMonth(monthMoment.format('MMMM').toLocaleUpperCase())
    } else {
      console.log('El mes proporcionado no es válido.')
    }

    setResetKey(prevResetKey => !prevResetKey)

    setSelectedMonthUp('')

    receiveListKardexMonth(newMonth)
  }

  const handleFileChange = (file) => {
    if (file && selectedMonthUp) {
      sendFileExcel(file, selectedMonthUp)

      setResetKey(prevReset => !prevReset)
      setResetKeyMonth(prevResetKey => !prevResetKey)
      setSelectedMonth('')
    }
  }

  const handleButtonClick = () => {
    const monthMoment = moment(selectedMonth, 'MM')
    if (monthMoment.isValid()) {
      setShowMonth(monthMoment.format('MMMM').toLocaleUpperCase())
    } else {
      console.log('El mes proporcionado no es válido.')
    }
    receiveListKardexMonth(selectedMonth)
  }

  const sendFileExcel = async (file, month) => {
    try {
      setLoading(true)
      const formData = new FormData()
      formData.append('file', file)
      formData.append('month', month)
      const respuesta = await fetch(`${ProjectConst.projectsUrl}/vaccines`, {
        method: 'POST',
        body: formData,
      })
      if (respuesta.ok) {
        const data = await respuesta.json()
        setListMonthVaccines(data)
        const monthMoment = moment(selectedMonthUp, 'MM')
        setShowMonth(monthMoment.format('MMMM').toLocaleUpperCase())
        setSelectedMonthUp('')
        setToastVisible(true)
        setToastMessage('Datos Subidos correctamente')
        setToastColor('success')
      }
    } catch (e) {
      console.log('algo salio mal', e)
      setToastVisible(true)
      setToastMessage('Error: ' + e.message)
      setToastColor('danger')
    }
    finally {
      setLoading(false)
    }
  }

  const receiveListKardexMonth = async (month) => {
    try {
      setLoading(true)
      const respuesta = await fetch(
        `${ProjectConst.projectsUrl}/vaccines/vaccines-month?month=${month}`,
        {
          method: 'GET',
        },
      )
      if (respuesta.ok) {
        const data = await respuesta.json()
        setListMonthVaccines(data)
        if (data.length > 0) {
          setToastVisible(true)
          setToastMessage('Datos de la Vacunas Traidos Correctamente')
          setToastColor('success')
        } else {
          setToastVisible(true)
          setToastMessage('Sin Datos en el Mes')
          setToastColor('warning')
        }
      } else {
        console.log('La respuesta no fue exitosa:', respuesta.status)
        setToastVisible(true)
        setToastMessage('Error: ' + respuesta.status)
        setToastColor('danger')
      }
    } catch (e) {
      console.log('algo salio mal', e)
      setToastVisible(true)
      setToastMessage('Error: ' + e.message)
      setToastColor('danger')
    }finally{
      setLoading(false)
    }
  }

  const handleCloseToast = () => {
    setToastVisible(false)
  }

  return (
    <>
     {loading && <Spinner />}
      {toastVisible && (
        <CToaster placement="top-end">
          <CustomToastComponent
            message={toastMessage}
            color={toastColor}
            visible={toastVisible}
            onClose={handleCloseToast}
          />
        </CToaster>
      )}

      <CRow>
        <CCol xs={12}>
          <CCard className="mb-4">
            <CCardHeader>KARDEX DE EXISTENCIA DE BIOLOGICOS</CCardHeader>
            <CCardBody>
              <CRow className="g-3">
                <CCol xs>
                  <MonthPicker onChange={handleMonthChangeUp} resetKey={resetKey}/>
                </CCol>
                <CCol xs>
                  <CustomFileInput
                    onFileSelected={handleFileChange}
                    multiple={false}
                    label="Subir Excel"
                    disabled={selectedMonthUp ? false : true}
                  />
                </CCol>
                <CCol xs>
                  <CCallout color="warning">
                    Cargue el archivo Excel, seleccionando el Mes.
                  </CCallout>
                </CCol>
              </CRow>

              <CCard className="mb-4">
                <CRow className="g-3">
                  <CCol xs>
                    <MonthPicker onChange={handleMonthChange} resetKey={resetKeyMonth}/>
                  </CCol>
                  <CCol xs></CCol>
                  <CCol xs>
                    <CCardBody className="d-flex justify-content-end">
                      <CustomButton
                        name="Buscar"
                        disabled={selectedMonth ? false : true}
                        onClick={handleButtonClick}
                      >
                        <CIcon icon={cilSearch} title="search" />
                      </CustomButton>
                    </CCardBody>
                  </CCol>
                </CRow>
              </CCard>

              <p className="text-body-secondary small">
                Lista de Vacunas del mes seleccionado <strong>{showMonth}</strong>
              </p>

              <CustomTabsVaccine data={listMonthVaccines} ></CustomTabsVaccine>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  )
}

export default Tables
